package com.pagecrawler.client;

import com.pagecrawler.shared.FieldVerifier;
import com.pagecrawler.shared.HtmlDocType;
import com.pagecrawler.shared.model.AnalysisResults;
import com.google.gwt.core.client.GWT;
import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;

/**
 * GWT JUnit <b>integration</b> tests must extend GWTTestCase.
 * Using <code>"GwtTest*"</code> naming pattern exclude them from running with
 * surefire during the test phase.
 * 
 * If you run the tests using the Maven command line, you will have to 
 * navigate with your browser to a specific url given by Maven. 
 * See http://mojo.codehaus.org/gwt-maven-plugin/user-guide/testing.html 
 * for details.
 */
public class GwtTestHtmlPageCrawler extends GWTTestCase {

  /**
   * Must refer to a valid module that sources this class.
   */
  public String getModuleName() {
    return "com.pagecrawler.HtmlPageCrawlerJUnit";
  }

  /**
   * Tests the FieldVerifier.
   */
  public void testFieldVerifier() {
    assertFalse(FieldVerifier.isValidUrl(null));
    assertFalse(FieldVerifier.isValidUrl(""));
    assertFalse(FieldVerifier.isValidUrl("a"));
    assertFalse(FieldVerifier.isValidUrl("ab"));
    assertFalse(FieldVerifier.isValidUrl("info@example.com"));
    assertTrue(FieldVerifier.isValidUrl("http://en.wikipedia.org/"));
  }

  /**
   * This test will send a request to the server using the greetServer method in
   * GreetingService and verify the response.
   */
  public void testGreetingService() {
    // Create the service that we will test.
    PageCrawlerServiceAsync greetingService = GWT.create(PageCrawlerService.class);
    ServiceDefTarget target = (ServiceDefTarget) greetingService;
    target.setServiceEntryPoint(GWT.getModuleBaseURL() + "HtmlPageCrawler/crawl");

    // Since RPC calls are asynchronous, we will need to wait for a response
    // after this test method returns. This line tells the test runner to wait
    // up to 10 seconds before timing out.
    delayTestFinish(10000);

    // Send a request to the server.
    greetingService.analyzeUrl("http://en.wikipedia.org/", new AsyncCallback<AnalysisResults>() {
      public void onFailure(Throwable caught) {
        // The request resulted in an unexpected error.
        fail("Request failure: " + caught.getMessage());
      }

      public void onSuccess(AnalysisResults result) {
        // Verify that the response is correct.
        assertTrue(result.getDocumentType().equals(HtmlDocType.HTML5));

        // Now that we have received a response, we need to tell the test runner
        // that the test is complete. You must call finishTest() after an
        // asynchronous test finishes successfully, or the test will time out.
        finishTest();
      }
    });
  }


}
