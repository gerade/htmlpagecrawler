package com.pagecrawler.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.pagecrawler.shared.AnalysisError;
import com.pagecrawler.shared.FieldVerifier;
import com.pagecrawler.shared.model.AnalysisResults;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class HtmlPageCrawler implements EntryPoint {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while "
	        + "attempting to contact the server. Please check your network " + "connection and try again.";

	/**
	 * Create a remote service proxy to talk to the server-side Greeting
	 * service.
	 */
	private final PageCrawlerServiceAsync crawlerService = GWT.create(PageCrawlerService.class);

	private final Messages messages = GWT.create(Messages.class);

	final Button analyzeButton = new Button(messages.analyzeButton());
	final TextBox urlField = new TextBox();
	final Label enterAddressLabel = new Label();
	final Label errorLabel = new Label();

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		enterAddressLabel.setText(messages.enterAddress());
		urlField.setText(messages.urlField());
		urlField.setStylePrimaryName("url-field");
		analyzeButton.addStyleName("analyzeButton");

		RootPanel.get("enterAddressLabelContainer").add(enterAddressLabel);
		RootPanel.get("urlFieldContainer").add(urlField);
		RootPanel.get("analyzeButtonContainer").add(analyzeButton);
		RootPanel.get("errorLabelContainer").add(errorLabel);

		// Focus the cursor on the site URL field when the app loads
		urlField.setFocus(true);
		urlField.selectAll();

		UserInputHandler handler = new UserInputHandler();
		analyzeButton.addClickHandler(handler);
		urlField.addKeyUpHandler(handler);
	}

	/**
	 * User input handler.
	 * 
	 * @author igor.polietaiev
	 * 
	 */
	class UserInputHandler implements ClickHandler, KeyUpHandler {
		/**
		 * Fired when the user clicks on the analyzeButton.
		 */
		public void onClick(ClickEvent event) {
			sendNameToServer();
		}

		/**
		 * Fired when the user types in the urlField.
		 */
		public void onKeyUp(KeyUpEvent event) {
			if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
				sendNameToServer();
			}
		}

		/**
		 * Send the name from the nameField to the server and wait for a
		 * response.
		 */
		private void sendNameToServer() {
			// First, we validate the input.
			errorLabel.setText("");
			String textToServer = urlField.getText();
			if (!FieldVerifier.isValidUrl(textToServer)) {
				errorLabel.setText(messages.checkUrl());
				return;
			}

			// Then, we send the input to the server.
			analyzeButton.setEnabled(false);
			RootPanel.get("analysisResultsContainer").clear();
			RootPanel.get("analysisResultsContainer").add(new HTML(messages.analysisInProgress()));
			crawlerService.analyzeUrl(textToServer, new AsyncCallback<AnalysisResults>() {
				public void onFailure(Throwable caught) {
					errorLabel.setText(SERVER_ERROR);
					analyzeButton.setEnabled(true);
				}

				public void onSuccess(AnalysisResults results) {
					RootPanel.get("analysisResultsContainer").clear();
					analyzeButton.setEnabled(true);

					if (results.getErrorCode() != null) {
						// display error message to the user
						AnalysisError code = results.getErrorCode();
						String errorMessage = "";
						if (AnalysisError.MALFORMED_URL_ERROR.equals(code) || AnalysisError.PROTOCOL_ERROR.equals(code)) {
							errorMessage = messages.checkUrl();
						} else if (AnalysisError.ACCESS_ERROR.equals(code) || AnalysisError.CONNECTION_ERROR.equals(code)) {
							errorMessage = messages.siteIsNotAccessible();
						} else if (AnalysisError.DOCUMENT_ANALYSIS_ERROR.equals(code)) {
							errorMessage = messages.documentAnalysisError();
						}
						errorMessage += messages.httpStatusCode() + results.getHttpStatusCode();
						errorLabel.setText(errorMessage);
					} else {
						// display results
						RootPanel.get("analysisResultsContainer").add(new ResultsView(messages, results).getWidget());
					}
				}
			});
		}
	}

}
