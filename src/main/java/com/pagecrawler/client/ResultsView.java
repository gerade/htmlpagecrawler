package com.pagecrawler.client;

import java.util.Map;

import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.pagecrawler.shared.model.AnalysisResults;

/**
 * Presents analysis results to the user or error message if analysis was
 * failed.
 * 
 * @author igor.polietaiev
 * 
 */
public class ResultsView {
	private VerticalPanel mainPanel = new VerticalPanel();

	public ResultsView(Messages messages, AnalysisResults results) {
		HTML resultsTitle = new HTML(messages.analysisResults());
		resultsTitle.setStylePrimaryName("results-title");
		mainPanel.add(resultsTitle);
		Grid resultsGrid = new Grid(12, 2);
		mainPanel.add(resultsGrid);
		mainPanel.setCellHorizontalAlignment(resultsTitle, HasHorizontalAlignment.ALIGN_CENTER);
		mainPanel.setCellHorizontalAlignment(resultsGrid, HasHorizontalAlignment.ALIGN_CENTER);

		resultsGrid.setWidget(0, 0, getColumnDesk(messages.htmlVersion()));
		resultsGrid.setWidget(0, 1, getColumnValue(results.getDocumentType().getHtmlVersionName()));

		resultsGrid.setWidget(1, 0, getColumnDesk(messages.pageTitle()));
		resultsGrid.setWidget(1, 1, getColumnValue(results.getPageTitle()));

		int i = 0;
		for (Map.Entry<String, Integer> entry : results.getHeadingLevelToNumberMap().entrySet()) {
			resultsGrid.setWidget(2 + i, 0, getColumnDesk(messages.headerLevel() + entry.getKey()));
			resultsGrid.setWidget(2 + i, 1, getColumnValue(entry.getValue().toString()));
			i++;
		}

		resultsGrid.setWidget(8, 0, getColumnDesk(messages.numInternal()));
		resultsGrid.setWidget(8, 1, getColumnValue(results.getNumInternalLinks().toString()));

		resultsGrid.setWidget(9, 0, getColumnDesk(messages.numExternal()));
		resultsGrid.setWidget(9, 1, getColumnValue(results.getNumExternalLinks().toString()));

		resultsGrid.setWidget(10, 0, getColumnDesk(messages.numInaccessible()));
		resultsGrid.setWidget(10, 1, getColumnValue(results.getNumInaccessibleLinks().toString()));

		resultsGrid.setWidget(11, 0, getColumnDesk(messages.hasLoginForm()));
		resultsGrid.setWidget(11, 1, getColumnValue(results.getPageContainsLoginForm().toString()));

	}

	private Widget getColumnDesk(String text) {
		HTML label = new HTML(text);
		label.setStylePrimaryName("results-desc-column");
		return label;
	}

	private Widget getColumnValue(String text) {
		HTML label = new HTML(text);
		label.setStylePrimaryName("results-value-column");
		return label;
	}

	public Widget getWidget() {
		return mainPanel;
	}
}
