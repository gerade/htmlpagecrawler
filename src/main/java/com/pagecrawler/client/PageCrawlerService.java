package com.pagecrawler.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.pagecrawler.shared.model.AnalysisResults;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("crawl")
public interface PageCrawlerService extends RemoteService {
	AnalysisResults analyzeUrl(String name);
}
