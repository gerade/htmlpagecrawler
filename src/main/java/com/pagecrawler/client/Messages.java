package com.pagecrawler.client;

import com.google.gwt.safehtml.shared.SafeHtml;

/**
 * Interface to represent the messages contained in resource bundle:
 * /Users/igor/
 * Documents/workspace/HtmlPageCrawler/src/main/resources/com/pagecrawler
 * /client/Messages.properties'.
 */
public interface Messages extends com.google.gwt.i18n.client.Messages {

	/**
	 * Translated "Enter site URL".
	 * 
	 * @return translated "Enter site URL"
	 */
	@DefaultMessage("Web-Site URL")
	@Key("siteUrl")
	String urlField();

	/**
	 * Translated "Analyze".
	 * 
	 * @return translated "Analyze"
	 */
	@DefaultMessage("Analyze")
	@Key("analyzeUriButton")
	String analyzeButton();

	/**
	 * Translated "Please enter site address:".
	 * 
	 * @return translated "Please enter site address:"
	 */
	@DefaultMessage("Please enter site address:")
	@Key("enterAddress")
	String enterAddress();

	@DefaultMessage("Please wait, Web-Site analysis is in progress...")
	@Key("analysisInProgress")
	String analysisInProgress();

	@DefaultMessage("Malformed URL. Please check that url contains protocal name (http, https, etc...) and Web-Site address is correct.")
	@Key("checkUrl")
	String checkUrl();

	@DefaultMessage("Specified Web-Site is not accessible.")
	@Key("siteIsNotAccessible")
	String siteIsNotAccessible();

	@DefaultMessage("Analysis results:")
	@Key("analysisResults")
	String analysisResults();

	@DefaultMessage("HTML version")
	@Key("htmlVersion")
	String htmlVersion();

	@DefaultMessage("Page title")
	@Key("pageTitle")
	String pageTitle();

	@DefaultMessage("Number of headers level ")
	@Key("headerLevel")
	String headerLevel();

	@DefaultMessage("Number internal links")
	@Key("numInternal")
	String numInternal();

	@DefaultMessage("Number external links")
	@Key("numExternal")
	String numExternal();

	@DefaultMessage("Number inaccessible links")
	@Key("numInaccessible")
	String numInaccessible();

	@DefaultMessage("Does page contain login form")
	@Key("hasLoginForm")
	String hasLoginForm();

	@DefaultMessage("HTTP status code: ")
	@Key("httpStatusCode")
	String httpStatusCode();

	@DefaultMessage("Sorry we cannot analyize the document. Document format broken.")
	@Key("documentAnalysisError")
	String documentAnalysisError();
}
