package com.pagecrawler.shared;

import com.google.gwt.user.client.rpc.IsSerializable;


/**
 * Document versions. Contains DOCTYPE versionid to HTML version mapping.
 * Based on {@link http ://www.w3schools.com/tags/tag_doctype.asp}
 * 
 * @author Igor Polietaiev
 * 
 */
public enum HtmlDocType  implements IsSerializable {
	UNKNOWN("Unknown", "Unknown"), //
	HTML5("", "HTML 5"), //
	HTML401STRICT("-//W3C//DTD HTML 4.01//EN", "HTML 4.01 Strict"), //
	HTML401TRANSITIONAL("-//W3C//DTD HTML 4.01 Transitional//EN",
			"HTML 4.01 Transitional"), //
	HTML401FRAMESET("-//W3C//DTD HTML 4.01 Frameset//EN", "HTML 4.01 Frameset"), //
	XHTML10STRICT("-//W3C//DTD XHTML 1.0 Strict//EN", "XHTML 1.0 Strict"), //
	XHTML10TRANSITIONAL("-//W3C//DTD XHTML 1.0 Transitional//EN",
			"XHTML 1.0 Transitional"), //
	XHTML10FRAMESET("-//W3C//DTD XHTML 1.0 Frameset//EN", "XHTML 1.0 Frameset"), //
	XHTML10("-//W3C//DTD XHTML 1.1//EN", "XHTML 1.1");

	private String publicId;
	private String htmlVersionName;

	private HtmlDocType(String id, String versionName) {
		this.publicId = id;
		this.htmlVersionName = versionName;
	}

	public static HtmlDocType getByPublicId(String id) {
		if (id != null) {
			for (HtmlDocType type : values()) {
				if (id.equals(type.getPublicId())) {
					return type;
				}
			}
		}
		return UNKNOWN;
	}

	public String getPublicId() {
		return publicId;
	}

	public void setPublicId(String publicId) {
		this.publicId = publicId;
	}

	public String getHtmlVersionName() {
		return htmlVersionName;
	}

	public void setHtmlVersionName(String htmlVersionName) {
		this.htmlVersionName = htmlVersionName;
	}
}
