package com.pagecrawler.shared;

/**
 * <p>
 * FieldVerifier validates that the URL the user enters is valid.
 * </p>
 * @author Igor Polietaiev
 */
public class FieldVerifier {
	/** URL regex.*/
	private static String URL_REGEX = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";

	/**
	 * Verifies that the specified url is valid for our service.
	 * 
	 * @param url
	 *            the URL to validate
	 * @return true if valid, false if invalid
	 */
	public static boolean isValidUrl(String url) {
		if (url == null) {
			return false;
		}
		return url.matches(URL_REGEX);
	}
}
