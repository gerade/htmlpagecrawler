package com.pagecrawler.shared.model;

import java.util.Map;
import java.util.TreeMap;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.pagecrawler.shared.AnalysisError;
import com.pagecrawler.shared.HtmlDocType;

public class AnalysisResults implements IsSerializable {
	/** Analysis error code */
	private AnalysisError errorCode;
	/** Response code for GET request to the user link */
	private int httpStatusCode;
	/** Document version */
	private HtmlDocType documentType;
	/** Title of HTML page. */
	private String pageTitle;
	/**
	 * Contains information about number of heading on the page together with
	 * their level.
	 */
	private Map<String, Integer> headingLevelToNumberMap = new TreeMap<String, Integer>();
	/** Number of internal links on the page. */
	private Integer numInternalLinks;
	/** Number of external links on the page. */
	private Integer numExternalLinks;
	/** Number of inaccessible links on the page. */
	private Integer numInaccessibleLinks;
	/** Indicates whether the page contains login form or not. */
	private Boolean pageContainsLoginForm;

	public AnalysisResults(AnalysisError error) {
		this.errorCode = error;
	}

	public AnalysisResults() {
	}

	public AnalysisError getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(AnalysisError errorCode) {
		this.errorCode = errorCode;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public Map<String, Integer> getHeadingLevelToNumberMap() {
		return headingLevelToNumberMap;
	}

	public void setHeadingLevelToNumberMap(
			Map<String, Integer> headingLevelToNumberMap) {
		this.headingLevelToNumberMap = headingLevelToNumberMap;
	}

	public Integer getNumInternalLinks() {
		return numInternalLinks;
	}

	public void setNumInternalLinks(Integer numInternalLinks) {
		this.numInternalLinks = numInternalLinks;
	}

	public Integer getNumExternalLinks() {
		return numExternalLinks;
	}

	public void setNumExternalLinks(Integer numExternalLinks) {
		this.numExternalLinks = numExternalLinks;
	}

	public Integer getNumInaccessibleLinks() {
		return numInaccessibleLinks;
	}

	public void setNumInaccessibleLinks(Integer numInaccessibleLinks) {
		this.numInaccessibleLinks = numInaccessibleLinks;
	}

	public Boolean getPageContainsLoginForm() {
		return pageContainsLoginForm;
	}

	public void setPageContainsLoginForm(Boolean pageContainsLoginForm) {
		this.pageContainsLoginForm = pageContainsLoginForm;
	}

	public void setDocumentType(HtmlDocType docType) {
		documentType = docType;
	}

	public HtmlDocType getDocumentType() {
		return documentType;
	}

	public void putHeadingsCount(String headerType, int count) {
		headingLevelToNumberMap.put(headerType, count);
	}

	public int getHttpStatusCode() {
		return httpStatusCode;
	}

	public void setHttpStatusCode(int httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}

}
