package com.pagecrawler.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Enumeration of error types that can occur during document analysis.
 * 
 * @author Igor Polietaiev
 * 
 */
public enum AnalysisError implements IsSerializable {
	ACCESS_ERROR, PROTOCOL_ERROR, MALFORMED_URL_ERROR, CONNECTION_ERROR, DOCUMENT_ANALYSIS_ERROR

}
