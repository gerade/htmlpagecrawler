package com.pagecrawler.server;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.DocumentType;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.pagecrawler.client.PageCrawlerService;
import com.pagecrawler.shared.AnalysisError;
import com.pagecrawler.shared.FieldVerifier;
import com.pagecrawler.shared.HtmlDocType;
import com.pagecrawler.shared.model.AnalysisResults;

/**
 * The server side implementation of {@link PageCrawlerService}.
 * 
 * @author Igor Polietaiev
 */
@SuppressWarnings("serial")
public class PageCrawlerServiceImpl extends RemoteServiceServlet implements PageCrawlerService {

	private Logger log = LoggerFactory.getLogger(PageCrawlerServiceImpl.class);

	private static final int HTTP_RESPONSE_SUCCESS_LOWER_BOUND = 200;
	private static final int HTTP_RESPONSE_SUCCESS_HIGHER_BOUND = 300;
	private static final int HTTP_RESPONSE_REDIRECT_HIGHER_BOUND = 400;
	private static final Object PASSWORD_TYPE = "password";

	public AnalysisResults analyzeUrl(String input) {
		// Verify that the input is valid
		if (!FieldVerifier.isValidUrl(input)) {
			return new AnalysisResults(AnalysisError.MALFORMED_URL_ERROR);
		}

		AnalysisResults results = new AnalysisResults();
		int responseCode = getResponseCodeForGetRequest(results, input);

		boolean isResponseCodeOk = isHttpResponseSucceed(responseCode);
		if (isResponseCodeOk) {
			// analyze url
			crawlUrl(results, input);
		} else {
			// url is not accessible
			results.setErrorCode(AnalysisError.ACCESS_ERROR);
		}
		results.setHttpStatusCode(responseCode);
		return results;
	}

	/**
	 * Analyzes content of the document for a given URL.
	 * 
	 * @param inputUrl
	 *            document URL
	 */
	private void crawlUrl(AnalysisResults results, String inputUrl) {
		Document doc;
		try {
			doc = Jsoup.connect(inputUrl).userAgent("Mozilla").get();

			// fetch HTML version
			HtmlDocType docType = HtmlDocType.UNKNOWN;
			List<Node> nods = doc.childNodes();
			for (Node node : nods) {
				// doctype should be the first node
				if (node instanceof DocumentType) {
					DocumentType documentType = (DocumentType) node;
					docType = HtmlDocType.getByPublicId(documentType.attr("publicId"));
					break;
				}
			}
			results.setDocumentType(docType);

			// select title
			results.setPageTitle(doc.title());

			// select headings
			Elements hTags = doc.select("h1, h2, h3, h4, h5, h6");
			Elements h1Tags = hTags.select("h1");
			Elements h2Tags = hTags.select("h2");
			Elements h3Tags = hTags.select("h3");
			Elements h4Tags = hTags.select("h4");
			Elements h5Tags = hTags.select("h5");
			Elements h6Tags = hTags.select("h6");
			results.putHeadingsCount("h1", h1Tags.size());
			results.putHeadingsCount("h2", h2Tags.size());
			results.putHeadingsCount("h3", h3Tags.size());
			results.putHeadingsCount("h4", h4Tags.size());
			results.putHeadingsCount("h5", h5Tags.size());
			results.putHeadingsCount("h6", h6Tags.size());

			// Fetch links and do not check duplicates
			Set<String> pageLinks = new HashSet<String>();
			int numInternalLinks = 0;
			int numExternalLinks = 0;
			// skipped as it is not a link to other document
			// Elements media = doc.select("[src]");
			Elements allElementsWithLinks = new Elements();
			allElementsWithLinks.addAll(doc.select("a[href]"));
			allElementsWithLinks.addAll(doc.select("link[href]"));

			for (Element element : allElementsWithLinks) {
				try {
					String urlString = element.attr("href");
					URI uri = new URI(urlString);
					if (!uri.isAbsolute()) {
						numInternalLinks++;
					} else {
						numExternalLinks++;
					}
					pageLinks.add(element.absUrl("href"));
				} catch (URISyntaxException e) {
					log.debug("Document contains invalid link. Document: {}, invalid link: {} ", inputUrl,
					        element.attr("href"));
				}
			}

			results.setNumInternalLinks(numInternalLinks);
			results.setNumExternalLinks(numExternalLinks);

			int numInaccessibleLinks = 0;
			for (String pageLink : pageLinks) {
				if (!checkIfLinkIsAccessible(pageLink)) {
					numInaccessibleLinks++;
				}
			}
			results.setNumInaccessibleLinks(numInaccessibleLinks);

			// check if login form present
			// if some form has input element of type password then it is
			// treated as login form
			boolean hasLoginForm = false;
			Elements forms = doc.select("form");
			for (Element form : forms) {
				Elements inputElements = form.select("input");
				for (Element inputElement : inputElements) {
					if (PASSWORD_TYPE.equals(inputElement.attr("type"))) {
						hasLoginForm = true;
						break;
					}
				}
				if (hasLoginForm) {
					break;
				}
			}
			results.setPageContainsLoginForm(hasLoginForm);

		} catch (IOException e) {
			results.setErrorCode(AnalysisError.DOCUMENT_ANALYSIS_ERROR);
			log.debug("Error during document analysis. Document url: {}, Exception message: {}", inputUrl,
			        e.getLocalizedMessage());
		}

	}

	/**
	 * Checks response code. <br>
	 * 2xx - success <br>
	 * 3xx - redirect <br>
	 * 4xx - client error <br>
	 * 5xx - server error <br>
	 * 
	 * @param responseCode
	 * @return
	 */
	private boolean isHttpResponseSucceed(int responseCode) {
		boolean result = false;
		if (responseCode >= HTTP_RESPONSE_SUCCESS_LOWER_BOUND && responseCode < HTTP_RESPONSE_SUCCESS_HIGHER_BOUND) {
			result = true;
		}
		return result;
	}

	/**
	 * Check if link is accessible.
	 * 
	 * @param pageLink
	 *            document url
	 * @return true if link is accessible (http status code is in succeed
	 *         range). Note that links that follow to redirect are treated as
	 *         accessible.
	 */
	private boolean checkIfLinkIsAccessible(String pageLink) {
		boolean result = false;
		int code = getResponseCodeForGetRequest(null, pageLink);
		if (code >= HTTP_RESPONSE_SUCCESS_LOWER_BOUND && code < HTTP_RESPONSE_REDIRECT_HIGHER_BOUND) {
			result = true;
		}
		return result;
	}

	/**
	 * Make HTTP GET request and return HTTP status code of response.
	 * 
	 * @param analysisResults
	 *            results tuple
	 * @param pageLink
	 *            link to check
	 * @return HTTP status code
	 */
	private int getResponseCodeForGetRequest(AnalysisResults analysisResults, String pageLink) {
		log.debug("Checking url: {}", pageLink);
		int code = 0;
		try {
			URL u = new URL(pageLink);
			HttpURLConnection huc = (HttpURLConnection) u.openConnection();
			huc.setRequestMethod("GET");
			huc.setDoOutput(true);
			code = huc.getResponseCode();
		} catch (ProtocolException e) {
			if (analysisResults != null) {
				analysisResults.setErrorCode(AnalysisError.PROTOCOL_ERROR);
			}
			log.debug("Exception during GET request to the URL {}. Exception message: {} ", pageLink,
			        e.getLocalizedMessage());
		} catch (MalformedURLException e) {
			if (analysisResults != null) {
				analysisResults.setErrorCode(AnalysisError.MALFORMED_URL_ERROR);
			}
			log.debug("Exception during GET request to the URL {}. Exception message: {} ", pageLink,
			        e.getLocalizedMessage());
		} catch (IOException e) {
			if (analysisResults != null) {
				analysisResults.setErrorCode(AnalysisError.CONNECTION_ERROR);
			}
			log.debug("Exception during GET request to the URL {}. Exception message: {} ", pageLink,
			        e.getLocalizedMessage());
		}
		return code;
	}

}
